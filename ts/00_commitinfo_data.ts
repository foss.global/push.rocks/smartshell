/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartshell',
  version: '3.0.3',
  description: 'shell actions designed as promises'
}
